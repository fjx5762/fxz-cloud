package com.fxz.common.security.service.user;

import com.fxz.common.core.constant.SecurityConstants;
import com.fxz.common.security.entity.FxzAuthUser;
import com.fxz.common.security.service.FxzUserDetailsService;
import com.fxz.common.security.util.SecurityUtil;
import com.fxz.system.entity.SystemUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author Fxz
 * @version 1.0
 * @date 2021-11-27 16:18
 */
@Slf4j
@Service("fxzUserDetailServiceImpl")
@RequiredArgsConstructor
public class FxzUserDetailServiceImpl implements FxzUserDetailsService {

	private final FxzUserManager fxzUserManager;

	/**
	 * 通过用户名从数据库中获取用户信息SystemUser和用户权限集合
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if (SecurityUtil.getAuthType() == null) {
			// 系统用户，认证方式通过用户名 username 认证
			return getUserDetails(fxzUserManager.findByName(username));
		}
		else {
			// 系统用户，认证方式通过用户名 手机号 认证
			return this.loadUserByMobile(username);
		}
	}

	@Override
	public UserDetails loadUserByMobile(String mobile) {
		return getUserDetails(fxzUserManager.findByMobile(mobile));
	}

	private UserDetails getUserDetails(SystemUser systemUser) {
		if (Objects.nonNull(systemUser)) {
			String permissions = fxzUserManager.findUserPermissions(systemUser.getUsername());

			boolean notLocked = false;
			if (StringUtils.equals(SystemUser.STATUS_VALID, systemUser.getStatus())) {
				notLocked = true;
			}

			FxzAuthUser authUser = new FxzAuthUser(systemUser.getUsername(), systemUser.getPassword(), true, true, true,
					notLocked, AuthorityUtils.commaSeparatedStringToAuthorityList(permissions));

			BeanUtils.copyProperties(systemUser, authUser);
			return authUser;
		}

		return null;
	}

	/**
	 * 是否支持此客户端校验
	 * @param clientId 目标客户端
	 * @return true/false
	 */
	@Override
	public boolean support(String clientId, String grantType) {
		return SecurityConstants.ADMIN_CLIENT_ID.equals(clientId);
	}

}
